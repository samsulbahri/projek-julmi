<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {

    function __construct(){
        parent::__construct();
            //validasi jika user belum login
           $this->data['CI'] =& get_instance();
           $this->load->helper(array('form', 'url'));
           $this->load->model('M_Admin');
           $this->load->library(array('cart'));
           if($this->session->userdata('masuk_sistem_rekam') != TRUE){
               $url=base_url('login');
               redirect($url);
           }
        }

    public function index() {
        $this->data['title_web'] = 'Laporan';
        $this->data['idbo'] = $this->session->userdata('ses_id');        

        $this->load->view('header_view',$this->data);
		$this->load->view('sidebar_view',$this->data);
        $this->load->view('laporan/laporan_view',$this->data);
		$this->load->view('footer_view',$this->data);
    }     
    
    
    public function laporan_pdf(){

		$data = array(
			"dataku" => array(
				"nama" => "Petani Kode",
				"url" => "http://petanikode.com"
			)
		);

        $this->data['user'] = $this->M_Admin->get_table('tbl_login');
	
		$this->load->library('pdf');
	
		$this->pdf->setPaper('A4', 'potrait');
		$this->pdf->filename = "laporan-petanikode.pdf";
		$this->pdf->load_view('laporan/laporan_pdf', $this->data);
	
	
	}

}