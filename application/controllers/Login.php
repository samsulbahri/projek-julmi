<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        //validasi jika user belum login
        $this->data['CI'] = &get_instance();
        $this->load->helper(array('form', 'url'));
        $this->load->model('M_login');

        $katalog = $this->db->get_where("tbl_rak", array("status" => 1));

        if ($katalog->num_rows() > 0) {
            $katalog = $katalog->result();

            foreach ($katalog as $k) {
                if ($k->waktu_hapus == date("Y-m-d")) {
                    $this->db->where('id_rak', $k->id_rak);
                    $this->db->delete('tbl_rak');
                }
            }
        }

        $user = $this->db->get("tbl_login");

        if ($user->num_rows() > 0) {
            $user = $user->result();

            foreach ($user as $u) {
                if ($u->tgl_berakhir == date("Y-m-d")) {
                    $this->db->where('id_login', $u->id_login);
                    $this->db->delete('tbl_login');
                }
            }
        }
    }
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {
        $this->data['title_web'] = 'Login | Digital Library';
        $this->load->view('login_view', $this->data);
    }

    public function auth()
    {
        $user = htmlspecialchars($this->input->post('user', TRUE), ENT_QUOTES);
        $pass = htmlspecialchars($this->input->post('pass', TRUE), ENT_QUOTES);
        // auth
        $proses_login = $this->db->query("SELECT * FROM tbl_login WHERE user='$user' AND pass = md5('$pass')");
        $row = $proses_login->num_rows();
        if ($row > 0) {
            $hasil_login = $proses_login->row_array();

            // create session
            $this->session->set_userdata('masuk_sistem_rekam', TRUE);
            $this->session->set_userdata('level', $hasil_login['level']);
            $this->session->set_userdata('ses_id', $hasil_login['id_login']);
            $this->session->set_userdata('anggota_id', $hasil_login['anggota_id']);

            if ($hasil_login['level'] == 'Anggota') {
                $data = array(
                    'id_login' => $hasil_login['id_login'],
                    'waktu' => date('Y-m-d H:i:s')
                );
                $this->db->insert('tbl_daftar_hadir', $data);
            }

            echo '<script>window.location="' . base_url() . 'dashboard";</script>';
        } else {

            echo '<script>alert("Login Gagal, Periksa Kembali Username dan Password Anda");
            window.location="' . base_url() . '"</script>';
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();
        echo '<script>window.location="' . base_url() . '";</script>';
    }
}
