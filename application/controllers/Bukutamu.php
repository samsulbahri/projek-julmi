<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bukutamu extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        //validasi jika user belum login
        $this->data['CI'] = &get_instance();
        $this->load->helper(array('form', 'url'));
        $this->load->model('M_Admin');
        if ($this->session->userdata('masuk_sistem_rekam') != TRUE) {
            $url = base_url('login');
            redirect($url);
        }
    }

    public function index()
    {
        $this->data['title_web'] = 'Buku Tamu';
        $this->data['idbo'] = $this->session->userdata('ses_id');
        $this->data['bukutamu'] = $this->db->query("SELECT * FROM tbl_daftar_hadir tdh, tbl_login tl WHERE tdh.id_login = tl.id_login ORDER BY tl.id_login")->result();

        $this->load->view('header_view', $this->data);
        $this->load->view('sidebar_view', $this->data);
        $this->load->view('bukutamu/bukutamu_view', $this->data);
        $this->load->view('footer_view', $this->data);
    }
}
