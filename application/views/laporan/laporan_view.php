<?php if (!defined('BASEPATH')) exit('No direct script acess allowed'); ?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <i class="fa fa-edit" style="color:green"> </i> <?= $title_web; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('dashboard'); ?>"><i class="fa fa-dashboard"></i>&nbsp; Dashboard</a></li>
      <li class="active"><i class="fa fa-file-text"></i>&nbsp; <?= $title_web; ?></li>
    </ol>
  </section>
  <section class="content">
    <?php if (!empty($this->session->flashdata())) {
      echo $this->session->flashdata('pesan');
    } ?>
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border"><?php if ($this->session->userdata('level') == 'Petugas') { ?>
              <a href="<?= base_url('laporan/laporan_pdf') ?>"><button class="btn btn-primary">
                  <i class="fa fa-download"> </i> Unduh Laporan Data Pengguna</button></a><?php } ?>

          </div>
          <!-- /.box- eader -->
        </div>
      </div>
    </div>
</div>
</section>
</div>