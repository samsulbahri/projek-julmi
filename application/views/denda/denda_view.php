<?php if (!defined('BASEPATH')) exit('No direct script acess allowed'); ?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			<i class="fa fa-edit" style="color:green"> </i> <?= $title_web; ?>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('dashboard'); ?>"><i class="fa fa-dashboard"></i>&nbsp; Dashboard</a></li>
			<li class="active"><i class="fa fa-file-text"></i>&nbsp; <?= $title_web; ?></li>
		</ol>
	</section>
	<section class="content">
		<?php if (!empty($this->session->flashdata())) {
			echo $this->session->flashdata('pesan');
		} ?>
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-sm-4">
						<div class="box box-primary">
							<div class="box-header with-border">
								<?php if (!empty($this->input->get('id'))) { ?>
									<h4> Edit Harga Denda</h4>
								<?php } else { ?>
									<h4> Tambah Harga Denda</h4>
								<?php } ?>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<?php if (!empty($this->input->get('id'))) { ?>
									<form method="post" action="<?= base_url('transaksi/dendaproses'); ?>">
										<div class="form-group">
											<label for="">Harga Denda</label>
											<input type="number" name="harga" value="<?= $den->harga_denda; ?>" class="form-control" placeholder="Contoh : 10000">

										</div>
										<div class="form-group">
											<label for="">Status</label>
											<select class="form-control" name="status">
												<option <?php if ($den->stat == 'Aktif') {
															echo 'selected';
														} ?>>Aktif</option>
												<option <?php if ($den->stat == 'Tidak Aktif') {
															echo 'selected';
														} ?>>Tidak Aktif</option>
											</select>
										</div>
										<br />
										<input type="hidden" name="edit" value="<?= $den->id_biaya_denda; ?>">
										<button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i> Edit Harga Denda</button>
									</form>
								<?php } else { ?>

									<form method="post" action="<?= base_url('transaksi/dendaproses'); ?>">
										<div class="form-group">
											<label for="">Harga Denda</label>
											<input type="number" name="harga" class="form-control" placeholder="Contoh : 10000">
										</div>
										<br />
										<input type="hidden" name="tambah" value="tambah">
										<button type="submit" class="btn btn-primary"> <i class="fa fa-plus"></i> Tambah Harga Denda</button>
									</form>
								<?php } ?>
							</div>
						</div>
					</div>
					<div class="col-sm-8">
						<div class="box box-primary">
							<div class="box-header with-border">
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<div class="table-responsive">
									<table id="example1" class="table table-bordered table-striped table" width="100%">
										<thead>
											<tr>
												<th>No</th>
												<th>Harga Denda</th>
												<th>Status</th>
												<th>Mulai Tanggal</th>
												<th>Aksi</th>
											</tr>
										</thead>
										<tbody>
											<?php $no = 1;
											foreach ($denda->result_array() as $isi) { ?>
												<tr>
													<td><?= $no; ?></td>
													<td><?= $isi['harga_denda']; ?></td>
													<td><?= $isi['stat']; ?></td>
													<td><?= $isi['tgl_tetap']; ?></td>
													<td style="width:20%;">
														<a href="<?= base_url('transaksi/denda?id=' . $isi['id_biaya_denda']); ?>"><button class="btn btn-success"><i class="fa fa-edit"></i></button></a>
														<?php if ($isi['stat'] == 'Aktif') { ?>
															<button class="btn btn-warning"><i class="fa fa-ban"></i></button>
														<?php } else { ?>
															<a href="<?= base_url('transaksi/dendaproses?denda_id=' . $isi['id_biaya_denda']); ?>" onclick="return confirm('Anda yakin Biaya denda ini akan dihapus ?');">
																<button class="btn btn-danger"><i class="fa fa-trash"></i></button></a>
														<?php } ?>
													</td>
												</tr>
											<?php $no++;
											} ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php if (!empty($this->session->flashdata())) {
			echo $this->session->flashdata('pesan');
		} ?>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-body">
						<br />
						<div class="table-responsive">
							<table id="example1" class="table table-bordered table-striped table" width="100%">
								<thead>
									<tr>
										<th>No</th>
										<th>No Pinjam</th>
										<th>ID Anggota</th>
										<th>Nama</th>
										<th>Pinjam</th>
										<th>Balik</th>
										<th style="width:10%">Status</th>
										<th>Denda</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$no = 1;
									foreach ($pinjam->result_array() as $isi) {
										$anggota_id = $isi['anggota_id'];
										$ang = $this->db->query("SELECT * FROM tbl_login WHERE anggota_id = '$anggota_id'")->row();

										$pinjam_id = $isi['pinjam_id'];
										$denda = $this->db->query("SELECT * FROM tbl_denda WHERE pinjam_id = '$pinjam_id'");
										$total_denda = $denda->row();
									?>
										<?php
										$jml = $this->db->query("SELECT * FROM tbl_pinjam WHERE pinjam_id = '$pinjam_id'")->num_rows();
										$date1 = date('Ymd');
										$date2 = preg_replace('/[^0-9]/', '', $isi['tgl_balik']);
										$diff = $date1 - $date2;

										if ($diff > 0) { ?>
											<tr>
												<td><?= $no; ?></td>
												<td><?= $isi['pinjam_id']; ?></td>
												<td><?= $isi['anggota_id']; ?></td>
												<td><?= $ang->nama; ?></td>
												<td><?= $isi['tgl_pinjam']; ?></td>
												<td><?= $isi['tgl_balik']; ?></td>
												<td><?= $isi['status']; ?></td>
												<td>
													<?php
													if ($isi['status'] == 'Di Kembalikan') {
														echo $this->M_Admin->rp($total_denda->denda);
													} else {
														echo $diff . ' hari';
														$dd = $this->M_Admin->get_tableid_edit('tbl_biaya_denda', 'stat', 'Aktif');
														echo '<p style="color:red;font-size:18px;">
												' . $this->M_Admin->rp($jml * ($dd->harga_denda * $diff)) . ' 
												</p><small style="color:#333;">* Untuk ' . $jml . ' Buku</small>';
													}
													?>
												</td>
											</tr>
										<?php } ?>

									<?php $no++;
									} ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>